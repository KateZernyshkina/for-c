#!/usr/bin/bash
cd $1
mkdir $3
answer=$(find . -depth -name '*.'$2)
for a in $answer
do
        cp $a $1/$3/
done
tar -czf $4 --absolute-names $3
echo "done"
